var url = "https://dip-chat-server.herokuapp.com/api/messages/";
var latestMsg = 0;
var users = [];

function User (navn, beskeder) {
    this.navn = navn;
    this.beskeder = beskeder;
}

$(document).ready(function() {
    setInterval(update, 500);
    $("button").on("click", function() {
        if ($("#message").val() != "") {
            $.post(url, {name: $("#name").val(), roomName: $("#roomName").val(), text: $("#message").val()})
                .done(function (response) {
                    update();
                    console.log(response);
                })
                .fail(function(error) {
                    console.log(error);
                })
        }
    });

    $("#roomName").on("input", function() {
        $("textarea").empty();
        users.splice(0,users.length);
        latestMsg = 0;
        update();
    });

    function update () {
        if ($("#roomName").val() != "") {
            var chatServer = url + $("#roomName").val() + "/" + latestMsg;
            $.getJSON(chatServer)
                .done(function (result) {
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].serial > latestMsg && result[i].roomName == $("#roomName").val()) {
                            latestMsg = result[i].serial;
                            $("#chatWindow").append(new Date(result[i].timestamp).toLocaleString() + ", " + result[i].name + ": " + result[i].text + " " + result[i].serial + " " + result[i].roomName + "\n");
                            if (users.length == 0) {
                                users.push(new User(result[i].name, 1));
                            } else {
                                var insert = true;
                                for (var j = 0; j < users.length; j++) {
                                    if (result[i].name == users[j].navn) {
                                        users[j].beskeder = users[j].beskeder + 1;
                                        insert = false;
                                    }
                                }
                                if (insert) {
                                    users.push(new User(result[i].name, 1));
                                }
                            }
                        }
                    }
                })
                .fail(function(arg) {
                    console.log(arg);
                });
            $("#statistik").empty();

            users.sort(function (a, b) {
                return b.beskeder - a.beskeder;
            });
            for (var k = 0; k < users.length; k++) {
                $("#statistik").append(users[k].navn + " beskeder: " + users[k].beskeder + "\n");
            }
        } else {
            $("textarea").empty();
            latestMsg = 0;
        }
    }
});