var exchangeRates = "https://api.fixer.io/latest";
var rateDKK = null;
var rateUSD = null;
var exchangeRate = null;

$(document).ready(function() {
    $.getJSON(exchangeRates)
        .done(function (result) {
            rateDKK = result.rates["DKK"];
            rateUSD = result.rates["USD"];
            exchangeRate = (rateDKK / rateUSD * 100).toFixed(4);
            $("#exchangeRate").text(exchangeRate);
        })
        .fail(function(arg) {
            console.log(arg);
        });

    $("#DKK").on("input", function() {
        if (!isNaN($("#DKK").val()) && $("#DKK").val() > 0) {
            $("#USD").val(($("#DKK").val() * 100 / exchangeRate).toFixed(4));
        } else {
            $("#USD").val("");
        }
    });

    $("#USD").on("input", function() {
        if (!isNaN($("#USD").val()) && $("#USD").val() > 0) {
            $("#DKK").val(($("#USD").val() / 100 * exchangeRate).toFixed(4));
        } else {
            $("#DKK").val("");
        }
    });
});
